const http = require('http');
const fs = require('fs');
const formidable = require('formidable');

// membuka file html
const html = fs.readFileSync('./upload.html');

// membuat server
const server = http.createServer(function(req,res){
    res.writeHead(200, {'Content-Type' : 'text/html'});

    if(req.method == 'GET'){
        res.end(html);
    }
    else if(req.method == 'POST'){
        // membuat objek dari kelas formidable.IncomingForm
        const form = new formidable.IncomingForm();

        form.parse(req, function(err, fields, files){
            // mengambil nama file temporari
            const tempFile = files.nama_file.path;

            // menentukan tujuan upload
            const destFile = './uploads/' + files.nama_file.name;

            // memindahkan file ke temporari tujuan
            var readStream=fs.createReadStream(tempFile);
            var writeStream=fs.createWriteStream(destFile);
            readStream.pipe(writeStream);
            readStream.on('end',function(){
            fs.unlinkSync(tempFile);
            if(err){
                res.end('<h1>Proses upload gagal!</h1>');
                throw err;
            }
            res.end('<h1>Proses upload berhasil</h1>');
            });
        })
        
    }
});

server.listen(3000);