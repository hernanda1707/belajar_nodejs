var http = require('http');

var server = http.createServer(function(req,res){
    res.setHeader('Content-Type','text/html');
    res.write('<html>');
    res.write('<head><title>My First Page</title></head>');

    if(req.url == '/'){
        res.write('<body><h1>HALAMAN UTAMA</h1></body>');
    }
    else if(req.url == '/katalog'){
        res.write('<body><h1>HALAMAN KATALOG</h1></body>');
    }
    else if(req.url == '/kontak'){
        res.write('<body><h1>HALAMAN KONTAK</h1></body>');
    }
    else{
        // status HTTP pada halaman yang tidak ditemukan
        res.status = 404;
        res.write('<body><h1>HALAMAN TIDAK DITEMUKAN! </h1></body>');
    }
    res.write('</html>');
    res.end();
});

server.listen(3000);