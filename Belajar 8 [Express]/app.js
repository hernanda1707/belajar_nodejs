const express = require('express');
const Sequelize = require('sequelize');


  const sequelize = new Sequelize('kfc', 'development', 'dkpIs01', {
    host: '103.18.133.253',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});

const category = sequelize.define('ms_category', {

    'id_category' : Sequelize.STRING,
    'Name_Category': Sequelize.STRING,
    'CreatedBy': Sequelize.STRING,
    'CreatedAt': Sequelize.STRING,
    'UpdatedBy':Sequelize.STRING,
    'UpdatedAt' : Sequelize.STRING,
    'Role_id' : Sequelize.INTEGER
}, {
    //prevent sequelize transform table name into plural
    freezeTableName: true,
});

const app = express()

app.get('/category/', (req, res) => {
    category.findAll(
        {
            attributes: ['id_category', 'Name_Category'],
            where : {Role_id : 1}
        }
    ).then(category => {
        res.json(category);
    })
})

app.listen(3000, () => console.log('aplikasi jalan di port 3000!'))