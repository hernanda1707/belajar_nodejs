// memanggil modul http
const http = require('http');

const qs = require('querystring');

const server = http.createServer(function(req,res){
    res.writeHead(200, {'Content-Type' : 'text/html'});

    if(req.url == '/'){
        if(req.method == 'GET'){
            res.end(
                '<h2> PENANGANAN FORM </h2><hr>' + 
                '<form action="/" method="post">' + 
                'Nama : <br> ' + 
                '<input type="text" name="nama"> <br><br>' + 
                'Email : <br>' + 
                '<input type="email" name="email"> <br><br>' + 
                '<input type="submit" value="Kirim">' + 
                '</form>'
            );
        }
        else if(req.method == 'POST'){
            var body = '';

            req.on('data', function(data){
                body = body+data;
            });

            req.on('end',function(){
                var form = qs.parse(body);

                res.end(
                    '<h2> Data yang dikirim: </h2><hr>' + 
                    'Nama : ' + form['nama'] + '<br>' + 
                    'Email : ' + form['email'] + '<hr>'
                );
            });
        }
        else{
            res.end('Metode pengiriman tidak dikenali!')
        }
    }
    else{
        res.end('HALAMAN TIDAK DITEMUKAN!')
    }
});

/* note : 
res.end() untuk mengakhiri response 
*/

server.listen(3000);