// memanggil modul http
const http = require('http');

// memanggil modul fs
const fs = require('fs');

// membuat fungsi panggilCSS
function panggilCSS(req,res){
    if(req.url == '/style.css'){
        res.writeHead(200,{'Content-Type' : 'text/css'});
        const fileContents = fs.readFileSync('./style.css', {encoding:'utf8'});
        res.write(fileContents);
        res.end();
    }
}

// membuat fungsi panggil HTML
function panggilHTML(path, res){
    fs.readFile(path, 'utf8', function(err, data){
        // jika error
        if(err){
            res.writeHead(404);
            res.write('<h1>File tidak ditemukan!</h1>');
        }
        else{
            res.write(data);
        }
        // untuk mengakhiri response 
        res.end();
    });
}

// membuat server
const server = http.createServer(function(req,res){
    res.writeHead(200,{'Content-Type' : 'text/html'});

    // memanggil fungsi panggil css
    panggilCSS(req,res);

    // memanggil fungsi panggilHTML
    panggilHTML('./index_css.html',res);  
});

server.listen(3000);