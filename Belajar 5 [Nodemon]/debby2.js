module.exports = (sequelize, Sequelize) => {
    const T_pendaftaran = sequelize.define('t_pendaftaran', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      tanggal: {
        type: Sequelize.DATE
      },
      antrean: {
        type: Sequelize.STRING
      },
      pasien_id: {
        type: Sequelize.STRING
      },
      umur_tahun: {
        type: Sequelize.INTEGER
      },
      umur_bulan: {
        type: Sequelize.INTEGER
      },
      umur_hari: {
        type: Sequelize.INTEGER
      },
      kunjungan: {
        type: Sequelize.STRING
      },
      with_pelayanan: {
        type: Sequelize.TINYINT
      },
      asuransi_id: {
        type: Sequelize.STRING
      },
      no_asuransi: {
        type: Sequelize.STRING
      },
      tarif: {
        type: Sequelize.DOUBLE
      },
      rujukan_dari: {
        type: Sequelize.STRING
      },
      nama_perujuk: {
        type: Sequelize.STRING
      },
      puskesmas_id: {
        type: Sequelize.STRING
      },
      created_at: {
        type: Sequelize.DATE
      },
      updated_at: {
        type: Sequelize.DATE
      },
      created_by: {
        type: Sequelize.STRING
      },
      updated_by: {
        type: Sequelize.STRING
      },
      reg_type: {
        type: Sequelize.TINYINT
      },
      status: {
        type: Sequelize.STRING
      },
      faskes_asal: {
        type: Sequelize.STRING
      }
    });
    
    return T_pendaftaran;
  }