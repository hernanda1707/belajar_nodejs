router.put('/register', ensureToken, function (req, res, next) {
    req.assert('puskesmas_id', 'puskesmas_id is required').notEmpty();
    req.assert('nik', 'nik is required').notEmpty();
    req.assert('asuransi_id', 'asuransi_id is required').notEmpty();
    req.assert('no_asuransi', 'no_asuransi is required').notEmpty();
    req.assert('nama', 'nama is required').notEmpty();
    req.assert('jenis_kelamin', 'jenis_kelamin is required').notEmpty();
    req.assert('tempat_lahir', 'tempat_lahir is required').notEmpty();
    req.assert('tanggal_lahir', 'tanggal_lahir is required').notEmpty();
    req.assert('gol_darah', 'gol_darah is required').notEmpty();
    req.assert('pekerjaan_id', 'pekerjaan_id is required').notEmpty();
    req.assert('agama', 'agama is required').notEmpty();
    req.assert('warganegara', 'warganegara is required').notEmpty();
    req.assert('alamat', 'alamat is required').notEmpty();
    req.assert('rt', 'rt is required').notEmpty();
    req.assert('rw', 'rw is required').notEmpty();
    req.assert('propinsi_id', 'propinsi_id is required').notEmpty();
    req.assert('kota_id', 'kota_id is required').notEmpty();
    req.assert('kecamatan_id', 'kecamatan_id is required').notEmpty();
    req.assert('kelurahan_id', 'kelurahan_id is required').notEmpty();
    req.assert('status_perkawinan', 'status_perkawinan is required').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        var error_msg = ''
        errors.forEach(function(error) {
            error_msg += error.msg + '\n'
        })                
        console.log(error_msg);
        res.json({
            'res' : 'error',
            'message' : error_msg
        });
    }else{
        var id              = req.sanitizeHeaders('id').trim();
        var puskesmas_id    = req.body.puskesmas_id.trim();
        var nik             = req.body.nik.trim();
        var asuransi_id     = req.body.asuransi_id.trim();
        var no_asuransi     = req.body.no_asuransi.trim();
        var nama            = req.body.nama.trim();
        var jenis_kelamin   = req.body.jenis_kelamin.trim();
        var tempat_lahir    = req.body.tempat_lahir.trim();
        var tanggal_lahir   = req.body.tanggal_lahir.trim();
        var gol_darah       = req.body.gol_darah.trim();
        var pekerjaan_id    = req.body.pekerjaan_id.trim();
        var agama           = req.body.agama.trim();
        var warganegara     = req.body.warganegara.trim();
        var alamat          = req.body.alamat.trim();
        var rt              = req.body.rt.trim();
        var rw              = req.body.rw.trim();
        var propinsi_id     = req.body.propinsi_id.trim();
        var kota_id         = req.body.kota_id.trim();
        var kecamatan_id    = req.body.kecamatan_id.trim();
        var kelurahan_id    = req.body.kelurahan_id.trim();
        var status_perkawinan = req.body.status_perkawinan.trim();

        var d  = new Date();
        var created_at = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
        var updated_at  = created_at;
        var verified_at = created_at;
        var created_by  = 'eAntrian';
        var verified_by = created_by;
        var updated_by  = created_by;

        M_puskesmas.findAll({
            limit: 1,
            where: {
                id: puskesmas_id
            }
        }).then(rows => {
            console.log(puskesmas_id+ ' - ' +created_at);

            connection_puskesmas = switch_db(puskesmas_id);
            var M_pasien   = connection_puskesmas.m_pasien;
            M_pasien.max('id', {
                where: {
                    puskesmas_id: {
                        [Op.startsWith]: puskesmas_id
                    }
                }
            }).then(id => {
                id = parseInt(id) + 1;
                var new_id = id.toString().padStart(16, '0');
                M_pasien.create({
                  id                : new_id,
                  nik               : nik,
                  asuransi_id       : asuransi_id,
                  no_bpjs           : no_asuransi,
                  nama_lengkap      : nama,
                  kelamin           : jenis_kelamin,
                  tempat_lahir      : tempat_lahir,
                  cl_bday           : tanggal_lahir,
                  pekerjaan_id      : pekerjaan_id,
                  agama             : agama,
                  warganegara       : warganegara,
                  alamat            : alamat,
                  rt                : rt,
                  rw                : rw,
                  propinsi_id       : propinsi_id,
                  kota_id           : kota_id,
                  kecamatan_id      : kecamatan_id,
                  kelurahan_id      : kelurahan_id,
                  puskesmas_id      : puskesmas_id,
                  created_at        : created_at,
                  updated_at        : updated_at,
                  verified_at       : verified_at,
                  created_by        : created_by,
                  updated_by        : updated_by,
                  verified_by       : verified_by
                }).then(result => {
                    console.log("insert M_pasien : " + new_id);

                    if(asuransi_id=="0000"){
                        var asuransi = "UMUM";
                        var content  = "Terima kasih <b>"+nama+"</b><br>Anda terdaftar sebagai pasien <b>UMUM<br><br>ID: <b style='font-size:40px'>"+new_id+"</b><br><br>Jika anda peserta <b>BPJS</b> silahkan konfirmasi ke petugas <b>LOKET</b> agar Nomor BPJS anda bisa di cek ulang. <br><br><button type='button' onClick='lanjut(0)' class='btn-lg btn-success'>LANJUTKAN PENDAFTARAN</button>";
                    }else{
                        var asuransi = "BPJS";
                        var content  = "Terima kasih <b>"+nama+"</b><br>Anda terdaftar sebagai pasien <b>BPJS<br><br>ID: <b>"+new_id+"</b><br><br>Nomor <b>BPJS</b> : "+no_asuransi+" <br><br><button type='button' onClick='lanjut(1)' class='btn-lg btn-success'>LANJUTKAN PENDAFTARAN</button>";
                    }

                    res.json({
                        'res'       : 'ok',
                        'status'    : asuransi,
                        'cl_pid'    : new_id,
                        'bpjs'      : no_asuransi,
                        'nama'      : nama,
                        'content'   : content,
                        'print'     : content
                    });

                }).catch(err => {
                    console.log(err.message);
                    res.json({
                        'res' : 'error',
                        'message' : err.message
                    });
                });
            });
        })
    }
})